# SSA 2021 kol-vezbanje

Projekat vezbanja za kolokvijum


Klonirati projekat sa adrese https://gitlab.com/nemanja.vicovic/ssa-2021-kol-vezbanje. Nakon kloniranja otvoriti projekat u editor-u (Visual Studio Code) i instalirati npm pakete (npm install). Pokrenuti projekat i rad. S obzirom da trajanje instalacije paketa može da potraje, moguće je početi radi dok traje instalacija (tek kada instalacija biće omogućeno pokretanje aplikacije).

Sa backend API adrese https://restcountries.eu/rest/v2/all?fields=name;capital;region;population;flag;numericCode "preuzeti" podatke i prikazati ih u po sopstvenom izboru stilizovanoj tabeli. U tabeli treba prikazati naziv države, region u kojoj se država nalazi, region i populaciju.

Koristiti sve standardne angular elemente: komponente, servise, modele...
